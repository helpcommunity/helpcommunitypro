# RESUMEN
### Este proyecto tiene como objetivo brindar información acerca de los objetos se que encuentren extraviados dentro de una comunidad, en donde se pretende implementar dicho sistema. A su vez mantener un orden y generar la cultura de honestidad de la ciudadana. Esto se pretende llevar a cabo con la implementación de un ChatBoot, en el cual va a hacer una ayuda para el manejo de la App, más la implementación de una base de datos “Firebase” dentro del cual se va a almacenar los datos que se vayan a manejar.


# PANTALLA LOGIN
### En esta pantalla login podemos visualizar varias opciones como usuario, las opciones presentadas a continuacion son: la primera le permite el ingreso a la aplicacion de un usuario ya registrado llenando los campos de E-mail y Password; la segunda es una opcion el cual el usuario no registrado le va a permitir redirigir a un formulario de registros por primera vez llenando todos los campos presentados y siendo un usuario mas en esta plataforma.

<img src="https://firebasestorage.googleapis.com/v0/b/cameracloud-586fd.appspot.com/o/WhatsApp%20Image%202020-10-07%20at%2017.25.56%20(1).jpeg?alt=media&token=fbfa8fa5-c9f2-401c-9948-21019676bac9" 
     alt="Size Limit logo by Anton Lovchikov" width="400" height="400">

# PANTALLA DE REGISTRO 
### En esta seccion se presenta un formulario de registros en general para los usuarios que no tienen una cuenta para el ingreso en la apliaccion. Los datos principales son nombres, apellidos, correo electronico (para la verificacion de registro) y contraseña como clave principal para el acceso y formar parte de ella

<img src="https://firebasestorage.googleapis.com/v0/b/cameracloud-586fd.appspot.com/o/WhatsApp%20Image%202020-10-07%20at%2017.25.56.jpeg?alt=media&token=d419ea9d-2343-415e-b4f0-0fc647a821e1" 
     alt="Size Limit logo by Anton Lovchikov" width="400" height="400">

# PANTALLA MENU PRINCIPAL 
### En esta seccion la aplicacion proporciona por medio de la interfaz  una experiencia al usuario de varias opciones y actividades, cada una de las opciones en panatalla presenta una informacion en general, el usuario puede navegar por la lista y seleccionar una tarea de acuerdo a  la activades que va a realizar; el menu principal cuenta con una lista de opciones como Mis objetos, Mascotas, Vehiculos, Chat, Reportes y la opcion salir una vez realizado el parte.

<img src="https://firebasestorage.googleapis.com/v0/b/cameracloud-586fd.appspot.com/o/WhatsApp%20Image%202020-10-07%20at%2017.25.56%20(2).jpeg?alt=media&token=00e0e497-a24c-47a3-a24e-a6a8f6e53b4d"
 alt="Size Limit logo by Anton Lovchikov" width="400" height="400">

 # PANTALLA REGISTRO DE OBJETOS 
 ### En esta pantalla el usuario puede detallar cada uno de los objetos, accesorios, mascotas y vehiculo en general extraviados, hurtados..ect. dando un detalle en general para que los usuarios registrados en esta apliacion puedan consualtar todos los acontecimientos, puedan encontrar sus objetos y ayudar a los demas con informacion necesaria. El usuario puede subir una foto de los objetos perdidos, nombre del articulo y una breve descripcion del lugar donde se ha encontrado.

<img src="https://firebasestorage.googleapis.com/v0/b/cameracloud-586fd.appspot.com/o/WhatsApp%20Image%202020-10-07%20at%2019.12.59.jpeg?alt=media&token=1ddefcfa-2aff-43ea-af3a-2804a6d6b324"
 alt="Size Limit logo by Anton Lovchikov" width="400" height="400">  

 # PANTALLA ASISTENTE IBM WATSON 
 ### Aqui los usuarios pueden consultar las dudas que tengan y esta aplicacion les brindara la informacion necesaria donde la  interaccion se realiza por medio de un ChatBot con preguntas y respuestas claves por defecto, de esta manera los usuarios pueden despejar todas sus dudas y conocer mucho mas la aplicacion de manera interactiva.

 <img src="https://firebasestorage.googleapis.com/v0/b/cameracloud-586fd.appspot.com/o/WhatsApp%20Image%202020-10-07%20at%2019.13.33.jpeg?alt=media&token=ab617db2-8280-460c-a86c-a84883a8a78d"
 alt="Size Limit logo by Anton Lovchikov" width="400" height="400">  


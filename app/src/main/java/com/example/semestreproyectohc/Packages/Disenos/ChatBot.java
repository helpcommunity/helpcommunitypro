package com.example.semestreproyectohc.Packages.Disenos;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

public class ChatBot extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
    }
}

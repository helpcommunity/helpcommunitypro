package com.example.semestreproyectohc.Packages.Actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.semestreproyectohc.Packages.Disenos.Chat;
import com.example.semestreproyectohc.Packages.Disenos.Mascota;
import com.example.semestreproyectohc.Packages.Disenos.Objetos;
import com.example.semestreproyectohc.Packages.Disenos.Reporte;
import com.example.semestreproyectohc.Packages.Disenos.Vehiculo;
import com.example.semestreproyectohc.Packages.Model.User;
import com.example.semestreproyectohc.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;

public class Menu extends AppCompatActivity {
  CardView salir,objeto,vehiculo,mascota,tabla,chat;
    FirebaseAuth mAuth;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        iniValues();
        obtenerDatos();
        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                startActivity(new Intent(Menu.this, Login.class));
            }
        });
        objeto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = mAuth.getCurrentUser().getUid();
                DocumentReference docRef = db.collection("Usuarios").document(id);
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                User user = document.toObject(User.class);
                                String i = document.getId();
                                Toast.makeText(getApplicationContext(), user.getEmail(), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(Menu.this, Objetos.class);
                                Bundle myBundle = new Bundle();
                                myBundle.putString("e", user.getEmail());
                                intent.putExtras(myBundle);
                                startActivity(intent);

                            } else {
                                Toast.makeText(getApplicationContext(), "NO HAY", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Fallo", Toast.LENGTH_LONG).show();

                        }


                    }
                });
            }
        });

    }

    public void iniValues(){

        salir=(CardView)findViewById(R.id.cardSalir);
        vehiculo=(CardView)findViewById(R.id.cardVehiculos);
        mascota=(CardView)findViewById(R.id.cardMascotas);
        objeto=(CardView)findViewById(R.id.cardObjetos);
        tabla=(CardView)findViewById(R.id.cardTabla);
        chat=(CardView)findViewById(R.id.cardChat);
        mAuth=FirebaseAuth.getInstance();
        db=FirebaseFirestore.getInstance();
    }


    public void obtenerDatos(){




    }
}
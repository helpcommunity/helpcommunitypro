package com.example.semestreproyectohc.Packages.Model;

import java.io.Serializable;

public class User implements Serializable {


    private String nombres;
    private String apellidos;
    private String email;
    private String password;
    private String tipo_usuario;



    public User() {

    }

    public User(String id, String nombres, String apellidos, String email, String password, String tipo_usuario) {

        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.password = password;
        this.tipo_usuario = tipo_usuario;

    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipo_usuario() {
        return tipo_usuario;
    }

    public void setTipo_usuario(String tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }



    @Override
    public String toString() {
        return "User{" +

                ", nombres='" + nombres + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", password='" + password + '\'' +
                ", tipo_usuario='" + tipo_usuario + '}';
    }
}
